GO ?= go
VERSION ?= $(shell git describe --tags --always | sed 's/-/+/' | sed 's/^v//')

.PHONY: build
build:
	$(GO) build -ldflags '-X "go.jolheiser.com/goly/static.Version=$(VERSION)"'

.PHONY: fmt
fmt:
	$(GO) fmt ./...

.PHONY: vet
vet:
	$(GO) vet ./...

.PHONY: test
test:
	$(GO) test -race ./...

.PHONY: clean
clean:
	rm -f static/css/goly.css*

.PHONY: sass
sass:
	npm run sass

.PHONY: goly
goly: build
	./goly --config config/goly.example.toml

.PHONY: pkg
pkg:
	pkger

.PHONY: npm
npm:
	npm install --no-save
	npm run build

.PHONY: build-all
build-all: npm generate pkg build