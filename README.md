# Goly

_Another_ URL shortener.

## Motivation

For some reason, URL shorteners seem to be my go-to when I want to play with
new things.

* [Chi](https://github.com/go-chi/chi)
* [Nord Theme](https://www.nordtheme.com/)
* [BBolt](https://github.com/etcd-io/bbolt)

## License

[MIT](LICENSE)

## Examples

![light](_examples/light.png)

![dark](_examples/dark.png)