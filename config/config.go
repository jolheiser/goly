package config

import (
	_ "embed"
	"io/ioutil"
	"os"
	"path"
	"strings"

	"github.com/pelletier/go-toml"
)

//go:embed goly.example.toml
var defaultConfig []byte

type Config struct {
	BaseURL  string `toml:"base_url"`
	Port     int    `toml:"port"`
	DBPath   string `toml:"db_path"`
	Cron     string `toml:"cron"`
	LogLevel string `toml:"log_level"`
	Links    []struct {
		URL   string `toml:"url"`
		Short string `toml:"short"`
	} `toml:"links"`
}

func Load(apn string) (*Config, error) {
	if apn == "" {
		configDir, err := os.UserConfigDir()
		if err != nil {
			return nil, err
		}
		apn = path.Join(configDir, "goly", "goly.toml")
	}

	if _, err := os.Stat(apn); os.IsNotExist(err) {
		if err := ioutil.WriteFile(apn, defaultConfig, os.ModePerm); err != nil {
			return nil, err
		}
	}

	var config Config
	tree, err := toml.LoadFile(apn)
	if err != nil {
		return nil, err
	}

	if err := tree.Unmarshal(&config); err != nil {
		return nil, err
	}

	if !path.IsAbs(config.DBPath) {
		config.DBPath = path.Join(path.Dir(apn), config.DBPath)
	}

	config.BaseURL = strings.TrimSuffix(config.BaseURL, "/")
	return &config, nil
}
