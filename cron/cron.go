package cron

import (
	"time"

	"go.jolheiser.com/goly/database"

	"go.jolheiser.com/beaver"
)

func Start(d time.Duration) {
	ticker := time.NewTicker(d)

	// Run at the start
	expire()

	for {
		<-ticker.C

		expire()

		ticker = time.NewTicker(d)
	}
}

func expire() {
	beaver.Debug("expiring links")
	links, err := database.GetLinks()
	if err != nil {
		beaver.Errorf("could not get link: %v", err)
	}

	if links != nil {
		now := time.Now()
		for _, link := range links {
			expiry := link.LastUsed.Add(time.Hour * 24 * 30)
			if link.Expiry != nil {
				expiry = *link.Expiry
			}

			if now.After(expiry) {
				beaver.Debugf("expiring link %s -> %s", link.Short, link.URL)
				if err := database.DeleteLink(link.Short); err != nil {
					beaver.Errorf("could not delete link %s: %v", link.Short, err)
				}
			}
		}
	}
}
