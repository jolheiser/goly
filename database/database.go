package database

import (
	"crypto/md5"
	"fmt"
	"os"
	"time"

	"go.etcd.io/bbolt"
)

var (
	bucketName = []byte("links")

	db *bbolt.DB
)

// Open populates the db variable for use
func Open(dbPath string) error {
	d, err := bbolt.Open(dbPath, os.ModePerm, bbolt.DefaultOptions)
	if err != nil {
		return err
	}

	if err := d.Update(func(tx *bbolt.Tx) error {
		if _, err := tx.CreateBucketIfNotExists(bucketName); err != nil {
			return err
		}
		return nil
	}); err != nil {
		return err
	}

	db = d
	return nil
}

// UniqueHash makes sure that the returned hash doesn't already exist in the DB
// seed is usually the link's original URL
func UniqueHash(seed string) (string, error) {
	var hash []byte

	if err := db.View(func(tx *bbolt.Tx) error {
		bucket := tx.Bucket(bucketName)

		hasher := md5.New()
		var unique bool
		for !unique {
			seed += fmt.Sprintf("%d", time.Now().Unix())
			if _, err := hasher.Write([]byte(seed)); err != nil {
				return err
			}

			hash = hasher.Sum(nil)
			if b := bucket.Get(hash); b == nil {
				unique = true
			}
		}
		return nil
	}); err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", hash[:3]), nil
}
