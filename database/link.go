package database

import (
	"encoding/json"
	"time"

	"go.etcd.io/bbolt"
)

type Link struct {
	Short    string     `json:"short"`
	URL      string     `json:"url"`
	Uses     int        `json:"uses"`
	Expiry   *time.Time `json:"expiry"`
	LastUsed time.Time  `json:"last_used"`
}

// SetLink creates or updates a link
func SetLink(link *Link) error {
	serial, err := json.Marshal(link)
	if err != nil {
		return err
	}

	return db.Update(func(tx *bbolt.Tx) error {
		return tx.Bucket(bucketName).Put([]byte(link.Short), serial)
	})
}

// GetLink returns a link by its hash
func GetLink(short string) (*Link, error) {
	var link *Link

	if err := db.View(func(tx *bbolt.Tx) error {
		serial := tx.Bucket(bucketName).Get([]byte(short))
		if serial == nil {
			return nil
		}
		return json.Unmarshal(serial, &link)
	}); err != nil {
		return nil, err
	}

	return link, nil
}

// DeleteLink removes a link
func DeleteLink(short string) error {
	return db.Update(func(tx *bbolt.Tx) error {
		return tx.Bucket(bucketName).Delete([]byte(short))
	})
}

// GetLinks returns all links
func GetLinks() ([]*Link, error) {
	var links []*Link
	if err := db.View(func(tx *bbolt.Tx) error {
		bucket := tx.Bucket(bucketName)
		links = make([]*Link, bucket.Stats().KeyN)
		var idx int
		return bucket.ForEach(func(_, v []byte) error {
			var link *Link
			if err := json.Unmarshal(v, &link); err != nil {
				return err
			}
			links[idx] = link
			idx++
			return nil
		})
	}); err != nil {
		return nil, err
	}

	return links, nil
}
