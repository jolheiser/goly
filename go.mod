module go.jolheiser.com/goly

go 1.16

require (
	github.com/go-chi/chi v4.1.1+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pelletier/go-toml v1.8.1
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	github.com/urfave/cli/v2 v2.2.0
	go.etcd.io/bbolt v1.3.4
	go.jolheiser.com/beaver v1.0.2
	go.jolheiser.com/overlay v0.0.2 // indirect
	go.jolheiser.com/rate v0.1.0
	golang.org/x/net v0.0.0-20200528225125-3c3fba18258b // indirect
)
