package main

import (
	"fmt"
	"net/http"
	"os"
	"path"
	"time"

	"go.jolheiser.com/goly/config"
	"go.jolheiser.com/goly/cron"
	"go.jolheiser.com/goly/database"
	"go.jolheiser.com/goly/router"

	"github.com/mitchellh/go-homedir"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

func main() {
	app := cli.NewApp()
	app.Name = "goly"
	app.Usage = "URL shortener"
	app.Flags = []cli.Flag{
		&cli.StringFlag{
			Name:    "config",
			Aliases: []string{"c"},
			Usage:   "Path to the config file for Goly to use",
		},
	}
	app.Action = runGoly

	if err := app.Run(os.Args); err != nil {
		beaver.Fatalf("an error occurred while running goly: %v", err)
	}
}

func runGoly(ctx *cli.Context) error {
	beaver.Console.Format = beaver.FormatOptions{
		TimePrefix:  true,
		StackPrefix: true,
		StackLimit:  15,
		LevelPrefix: true,
		LevelColor:  true,
	}

	home, err := homedir.Dir()
	if err != nil {
		return err
	}

	configPath := path.Join(home, "goly.toml")
	if ctx.IsSet("config") {
		configPath = ctx.String("config")
	}

	cfg, err := config.Load(configPath)
	if err != nil {
		return err
	}

	beaver.Console.Level = beaver.ParseLevel(cfg.LogLevel)

	if err := database.Open(cfg.DBPath); err != nil {
		return err
	}

	c, err := time.ParseDuration(cfg.Cron)
	if err != nil {
		return err
	}
	go cron.Start(c)

	mux, err := router.New(cfg)
	if err != nil {
		return err
	}

	port := fmt.Sprintf(":%d", cfg.Port)
	beaver.Infof("goly is running at http://localhost%s", port)
	return http.ListenAndServe(port, mux)
}
