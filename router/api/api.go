package api

import (
	"net/http"
	"regexp"
	"time"

	"go.jolheiser.com/goly/config"

	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	"go.jolheiser.com/rate"
)

var (
	cfg       *config.Config
	linkRe    = regexp.MustCompile(`[^\w-]+`)
	limit     = rate.NewTimeFrame(10, time.Minute)
	blocklist = []string{
		"api",
		"copy",
		"static",
	}
)

func New(c *config.Config) *chi.Mux {
	cfg = c
	r := chi.NewMux()
	r.Use(cors.AllowAll().Handler)

	r.Get("/status", getStatus)
	r.Get("/link/{short}", getLink)
	r.Get("/link/{short}/qr", getQR)
	r.With(rateLimit).Post("/link", createLink)

	return r
}

func rateLimit(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		if err := limit.Try(); err != nil {
			apiError(w, err)
			return
		}
		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}
