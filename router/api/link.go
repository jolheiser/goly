package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"time"

	"go.jolheiser.com/goly/database"

	"github.com/go-chi/chi"
	"go.jolheiser.com/beaver"
)

type Response struct {
	Status  int            `json:"status"`
	Message string         `json:"message,omitempty"`
	Link    *database.Link `json:"link,omitempty"`
}

type Link struct {
	Short  string     `json:"short"`
	URL    string     `json:"url"`
	Expiry *time.Time `json:"expiry"`
}

func getLink(w http.ResponseWriter, r *http.Request) {
	short := chi.URLParam(r, "short")

	// Check blocklist
	for _, b := range blocklist {
		if b == short {
			jsonResponse(w, Response{
				Status:  http.StatusForbidden,
				Message: "This short code is reserved.",
			})
			return
		}
	}

	// Check permanent URLs
	for _, l := range cfg.Links {
		if short == l.Short {
			jsonResponse(w, Response{
				Status: http.StatusOK,
				// Dummy Link
				Link: &database.Link{
					Short: l.Short,
					URL:   l.URL,
				},
			})
			return
		}
	}

	exists, err := database.GetLink(short)
	if err != nil {
		apiError(w, err)
		return
	}

	if exists == nil {
		jsonResponse(w, Response{
			Status:  http.StatusNotFound,
			Message: "No URL exists with this short code.",
		})
		return
	}

	jsonResponse(w, Response{
		Status: http.StatusOK,
		Link:   exists,
	})
}

func createLink(w http.ResponseWriter, r *http.Request) {
	var link *Link
	if err := json.NewDecoder(r.Body).Decode(&link); err != nil {
		apiError(w, err)
		return
	}

	if link.URL == "" {
		apiError(w, errors.New("URL cannot be blank"))
		return
	}
	if _, err := url.Parse(link.URL); err != nil {
		apiError(w, err)
		return
	}

	expiryMax := time.Now().Add(time.Hour * 24 * 31) // One extra day here to compensate for inclusion
	if link.Expiry != nil && link.Expiry.After(expiryMax) {
		apiError(w, fmt.Errorf("expiry cannot be customized past %s", expiryMax.Format("01/02/2006")))
		return
	}

	if link.Short == "" {
		hash, err := database.UniqueHash(link.URL)
		if err != nil {
			apiError(w, err)
			return
		}
		link.Short = hash
	} else {
		for _, b := range blocklist {
			if b == link.Short {
				jsonResponse(w, Response{
					Status:  http.StatusForbidden,
					Message: "This short code is reserved.",
				})
				return
			}
		}
		for _, l := range cfg.Links {
			if link.Short == l.Short {
				jsonResponse(w, Response{
					Status:  http.StatusConflict,
					Message: "A URL already exists with this short code.",
				})
				return
			}
		}

		if linkRe.MatchString(link.Short) {
			jsonResponse(w, Response{
				Status:  http.StatusBadRequest,
				Message: "A short code can only contain alphanumeric characters, hyphens, or underscores.",
			})
			return
		}

		exists, err := database.GetLink(link.Short)
		if err != nil {
			apiError(w, err)
			return
		}

		if exists != nil {
			jsonResponse(w, Response{
				Status:  http.StatusConflict,
				Message: "A URL already exists with this short code.",
			})
			return
		}
	}

	dbLink := &database.Link{
		Short:    link.Short,
		URL:      link.URL,
		Uses:     0,
		Expiry:   link.Expiry,
		LastUsed: time.Now(),
	}
	if err := database.SetLink(dbLink); err != nil {
		apiError(w, err)
		return
	}

	jsonResponse(w, Response{
		Status: http.StatusCreated,
		Link:   dbLink,
	})
}

func apiError(w http.ResponseWriter, err error) {
	beaver.Errorf("API Error: %v", err)
	jsonResponse(w, Response{
		Status:  http.StatusInternalServerError,
		Message: err.Error(),
	})
}

func jsonResponse(w http.ResponseWriter, r Response) {
	data, err := json.MarshalIndent(r, "", "\t")
	if err != nil {
		beaver.Errorf("could not marshal JSON: %v", err)
		data = []byte("{}")
	}
	w.WriteHeader(r.Status)
	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(data)
}
