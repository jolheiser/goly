package api

import (
	"fmt"
	"image/color"
	"net/http"
	"strings"

	"github.com/go-chi/chi"
	"github.com/skip2/go-qrcode"
)

func getQR(w http.ResponseWriter, r *http.Request) {
	linkURL := fmt.Sprintf("%s/%s", cfg.BaseURL, chi.URLParam(r, "short"))
	qr, err := qrcode.New(linkURL, qrcode.Medium)
	if err != nil {
		apiError(w, err)
		return
	}

	q := r.URL.Query()
	if q.Get("fg") != "" {
		if c, ok := hexColor(q.Get("fg")); ok {
			qr.ForegroundColor = c
		}
	}
	if q.Get("bg") != "" {
		if c, ok := hexColor(q.Get("bg")); ok {
			qr.BackgroundColor = c
		}
	}

	png, err := qr.PNG(256)
	if err != nil {
		apiError(w, err)
		return
	}

	w.Header().Set("Content-Type", "image/png")
	_, _ = w.Write(png)
}

func hexColor(s string) (c color.RGBA, ok bool) {
	ok = true

	// First check for special case
	if strings.EqualFold(s, "transparent") {
		c.A = 0
		return
	}

	c.A = 0xff

	hexToByte := func(b byte) byte {
		switch {
		case b >= '0' && b <= '9':
			return b - '0'
		case b >= 'a' && b <= 'f':
			return b - 'a' + 10
		case b >= 'A' && b <= 'F':
			return b - 'A' + 10
		}
		ok = false
		return 0
	}

	switch len(s) {
	case 6:
		c.R = hexToByte(s[0])<<4 + hexToByte(s[1])
		c.G = hexToByte(s[2])<<4 + hexToByte(s[3])
		c.B = hexToByte(s[4])<<4 + hexToByte(s[5])
	case 3:
		c.R = hexToByte(s[0]) * 17
		c.G = hexToByte(s[1]) * 17
		c.B = hexToByte(s[2]) * 17
	default:
		ok = false
	}
	return
}
