package api

import (
	"encoding/json"
	"net/http"

	"go.jolheiser.com/goly/database"
	"go.jolheiser.com/goly/static"

	"go.jolheiser.com/beaver"
)

type Status struct {
}

func getStatus(w http.ResponseWriter, r *http.Request) {
	links, err := database.GetLinks()
	if err != nil {
		apiError(w, err)
		return
	}

	resp := map[string]interface{}{
		"version": static.Version,
		"count":   len(links),
	}
	data, err := json.MarshalIndent(resp, "", "\t")
	if err != nil {
		beaver.Errorf("could not marshal JSON: %v", err)
		data = []byte("{}")
	}
	w.Header().Set("Content-Type", "application/json")
	_, _ = w.Write(data)
}
