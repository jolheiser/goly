package router

import (
	"net/http"

	"github.com/go-chi/chi"
)

func copyShort(w http.ResponseWriter, r *http.Request) {
	tpl, err := lookupTemplate("copy")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := tpl.Execute(w, map[string]string{
		"Short": chi.URLParam(r, "short"),
	}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
