package router

import (
	"net/http"
	"time"
)

func index(w http.ResponseWriter, r *http.Request) {
	tpl, err := lookupTemplate("index")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := tpl.Execute(w, map[string]string{
		"ExpiryMax": time.Now().Add(time.Hour * 24 * 30).Format("2006-01-02"),
	}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
