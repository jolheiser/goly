package router

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"go.jolheiser.com/goly/database"

	"github.com/go-chi/chi"
	"go.jolheiser.com/beaver"
)

func link(w http.ResponseWriter, r *http.Request) {
	short := chi.URLParam(r, "short")

	// First check permanent URLs
	for _, l := range cfg.Links {
		if short == l.Short {
			resolveRedirect(w, r, l.URL)
			return
		}
	}

	exists, err := database.GetLink(short)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if exists == nil {
		http.NotFound(w, r)
		return
	}

	if exists.Expiry != nil && time.Now().After(*exists.Expiry) {
		if err := database.DeleteLink(exists.Short); err != nil {
			beaver.Errorf("could not delete link %s: %v", exists.Short, err)
			return
		}
		http.NotFound(w, r)
		return
	}

	exists.LastUsed = time.Now()
	exists.Uses++
	if err := database.SetLink(exists); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resolveRedirect(w, r, exists.URL)
}

func resolveRedirect(w http.ResponseWriter, r *http.Request, url string) {
	extra := chi.URLParam(r, "*")
	resolved := url
	if extra != "" {
		resolved = fmt.Sprintf("%s/%s", strings.TrimSuffix(resolved, "/"), extra)
	}
	if r.URL.RawQuery != "" {
		resolved += fmt.Sprintf("?%s", r.URL.RawQuery)
	}
	if r.URL.Fragment != "" {
		resolved += fmt.Sprintf("#%s", r.URL.Fragment)
	}
	http.Redirect(w, r, resolved, http.StatusPermanentRedirect)
}
