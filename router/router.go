package router

import (
	"fmt"
	"html/template"
	"net/http"

	"go.jolheiser.com/goly/config"
	"go.jolheiser.com/goly/router/api"
	"go.jolheiser.com/goly/static"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

var (
	tmpl *template.Template
	cfg  *config.Config
)

// New returns a new router
func New(c *config.Config) (*chi.Mux, error) {
	cfg = c
	m := chi.NewMux()
	m.Use(middleware.Recoverer)

	// UI
	m.Get("/", index)
	m.Get("/copy/{short}", copyShort)

	// Link Redirect
	m.Route("/{short}", func(r chi.Router) {
		r.Get("/", link)
		r.Get("/*", link)
	})

	// Static
	m.Get("/static/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/static", http.FileServer(http.FS(static.Static))).ServeHTTP(w, r)
	})

	// API
	m.Mount("/api", api.New(c))

	var err error
	tmpl, err = static.Template(c)
	if err != nil {
		return nil, err
	}

	return m, nil
}

func lookupTemplate(name string) (*template.Template, error) {
	tpl := tmpl.Lookup(name)
	if tpl == nil {
		return nil, fmt.Errorf("no template found for %s", name)
	}
	return tpl, nil
}
