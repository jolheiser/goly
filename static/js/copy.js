(() => {
    const short = document.querySelector('#short');
    document.querySelector("#copy").addEventListener('click', () => {
        clipCopy(short);
    });

    const color = document.querySelector("html");
    console.log(document.querySelector("html").style, color);

    const qr = document.querySelector('#qr');
    qr.src = `${window.config.baseURL}/api/link/${short.getAttribute('data-short')}/qr?bg=transparent`
    qr.classList.remove('is-hidden');
})()

function clipCopy(element) {
    element.select();
    element.setSelectionRange(0, 99999);
    document.execCommand("copy");
}