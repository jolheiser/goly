let url;
let custom;
let expiry;
let shorten;

(() => {
    url = document.querySelector('#url');
    document.querySelector('#clean').addEventListener('click', cleanQuery);
    custom = document.querySelector('#custom');
    custom.addEventListener('change', checkCustom);
    expiry = document.querySelector('#expiry');
    expiry.addEventListener('change', checkDatePlaceholder);
    shorten = document.querySelector('#shorten');
    shorten.addEventListener('click', shortenAPI);
})()

function cleanQuery() {
    const url = document.querySelector(`#url`);

    // Query parameters
    const hasQuery = url.value.indexOf('?');
    url.value = url.value.substring(0, hasQuery > -1 ? hasQuery : url.value.length);

    // anchors
    const hasAnchor = url.value.indexOf('#');
    url.value = url.value.substring(0, hasAnchor > -1 ? hasAnchor : url.value.length);
}

function checkDatePlaceholder(event) {
    const target = event.target;
    if (target.value !== '') {
        target.classList.remove('placeholder');
    } else {
        target.classList.add('placeholder');
    }
}

const linkRe = /[^\w-]+/;

function checkCustom() {
    const check = document.querySelector("#custom-check").classList;
    check.add('is-hidden');
    const x = document.querySelector("#custom-x").classList;
    x.add('is-hidden');
    if (custom.value === '') return;

    // Validate
    if (linkRe.test(custom.value)) {
        x.remove('is-hidden');
        return;
    }

    const loading = document.querySelector('#custom-div').classList;
    loading.add('is-loading');
    ajax('GET', `${window.config.baseURL}/api/link/${custom.value}`, null, (status) => {
        loading.remove('is-loading');
        if (status === 404) {
            check.remove('is-hidden');
        } else {
            x.remove('is-hidden');
        }
    })
}

function shortenAPI() {
    shorten.classList.add('is-loading');
    const link = {
        short: custom.value,
        url: url.value,
    };
    if (expiry.value !== '') {
        link.expiry = new Date(expiry.value);
    }
    ajax('POST', `${window.config.baseURL}/api/link`, JSON.stringify(link), (status, data) => {
        shorten.classList.remove('is-loading');
        if (status !== 201) {
            checkCustom();
            return;
        }
        data = JSON.parse(data)
        location.href = `${window.config.baseURL}/copy/${data.link.short}`;
    });
}

function ajax(method, url, data, callback, onError) {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState === XMLHttpRequest.DONE) {   // XMLHttpRequest.DONE == 4
            callback(xmlhttp.status, xmlhttp.responseText);
        }
    };
    xmlhttp.open(method, url, true);
    xmlhttp.send(data);
}
