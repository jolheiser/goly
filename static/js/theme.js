let prefersDark;
let themeDark;
let themeLight;
let isDark;

(() => {
    themeDark = document.querySelector('#theme-dark');
    themeLight = document.querySelector('#theme-light');
    prefersDark = window.matchMedia('(prefers-color-scheme: dark)');
    document.querySelector('#select-theme').addEventListener('click', selectTheme);
    document.querySelector('#select-theme').addEventListener('contextmenu', (event) => {
        event.preventDefault();
        setCookie('goly_theme', '')
        initTheme();
    });
    initTheme();
})()

function selectTheme() {
    themeDark.classList.add('is-hidden');
    themeLight.classList.add('is-hidden');
    isDark = !isDark;
    checkTheme();
    setCookie('goly_theme', isDark ? 'dark' : 'light');
}

function initTheme() {
    const theme = getCookie('goly_theme');
    if (theme === '') {
        isDark = prefersDark.matches;
    } else {
        isDark = theme === 'dark';
    }
    checkTheme();
}

function checkTheme() {
    const css = document.querySelector('#theme');
    if (isDark) {
        themeDark.classList.remove('is-hidden');
        css.href = `${window.config.baseURL}/static/css/dark.css`;
    } else {
        themeLight.classList.remove('is-hidden');
        css.href = `${window.config.baseURL}/static/css/light.css`;
    }
}

function setCookie(cname, cvalue) {
    const d = new Date();
    d.setTime(d.getTime() + (365*24*60*60*1000)); // 1 year
    const expires = 'expires=' + d.toUTCString();
    document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
}

function getCookie(cname) {
    const name = cname + '=';
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');
    for(let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}