package static

import (
	"embed"
	"fmt"
	"html/template"
	"io"
	"io/fs"
	"os"
	"path"
	"strings"

	"go.jolheiser.com/goly/config"

	"go.jolheiser.com/overlay"
)

var (
	//go:embed templates
	templates embed.FS
	//go:embed css img js
	Static  embed.FS
	Version = "develop"
)

func Template(cfg *config.Config) (*template.Template, error) {
	customPath := os.Getenv("GOLY_CUSTOM")
	if customPath == "" {
		customPath = "/var/lib/goly/custom"
	}
	ofs, err := overlay.New(customPath, templates)
	if err != nil {
		return nil, err
	}

	tmpl := template.New("").Funcs(funcMap(cfg))

	if err := fs.WalkDir(ofs, "templates", func(walkPath string, walkInfo fs.DirEntry, walkErr error) error {
		if walkErr != nil {
			return walkErr
		}

		if walkInfo.IsDir() {
			return nil
		}

		fi, err := ofs.Open(walkPath)
		if err != nil {
			return fmt.Errorf("could not open %s: %v", walkInfo.Name(), err)
		}

		data, err := io.ReadAll(fi)
		if err != nil {
			return fmt.Errorf("could not read %s: %v", walkInfo.Name(), err)
		}

		if _, err := tmpl.New(strings.TrimSuffix(walkInfo.Name(), path.Ext(walkInfo.Name()))).Parse(string(data)); err != nil {
			return err
		}

		return nil
	}); err != nil {
		return nil, err
	}

	return tmpl, nil
}

func funcMap(cfg *config.Config) template.FuncMap {
	return template.FuncMap{
		"AppVer": func() string {
			return Version
		},
		"BaseURL": func() string {
			return cfg.BaseURL
		},
	}
}
